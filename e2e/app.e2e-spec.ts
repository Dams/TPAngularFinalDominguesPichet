import { TPAngularFinalDominguesPichetPage } from './app.po';

describe('tpangular-final-domingues-pichet App', () => {
  let page: TPAngularFinalDominguesPichetPage;

  beforeEach(() => {
    page = new TPAngularFinalDominguesPichetPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
