import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-rechercheform',
  templateUrl: './rechercheform.component.html',
  styleUrls: ['./rechercheform.component.css']
})
export class RechercheformComponent implements OnInit {

  ngOnInit() {
  }

  charNameInput: string;

  @Output() charName: EventEmitter<string> = new EventEmitter<string>();
  @Output() charNameForFav : EventEmitter<string> = new EventEmitter<string>();

  loadChar(){
    this.charName.emit(this.charNameInput);
  }

  addInFavoris() {
    this.charNameForFav.emit(this.charNameInput);
  }

}
