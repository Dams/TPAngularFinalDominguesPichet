import { Component, OnInit, Input } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class recherche {

  constructor(private http: Http) { }


  getChar(char: string) {
    return this.http.get(`https://gateway.marvel.com:443/v1/public/characters?name=` + char + `&apikey=297d76d46e29dc2afe710d49fe8e4c8c`)
      .map((res: Response) => res.json().data.results);
  }



}
