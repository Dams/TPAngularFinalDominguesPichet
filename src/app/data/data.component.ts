import { Component, OnInit } from '@angular/core';
import { recherche } from '../API/service/recherche';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {
constructor(private _serviceRecherche: recherche) { }

  title = 'API Marvel';

  heroes: Array<Object>;
  favoris: Array<string> = [];

  ngOnInit() {
    var storedNames = JSON.parse(localStorage.getItem("favoris"));
    for (var i = 0; i < storedNames.length; i++) {
      this.favoris.push(storedNames[i]);
    }
  }



  onHeroesSeach(charName: string) {
    this._serviceRecherche.getChar(charName).subscribe(data => this.heroes = data);
  }

  onAddFavoris(charNameForFav: string) {
    this.favoris.push(charNameForFav);
    localStorage.setItem("favoris", JSON.stringify(this.favoris));
  }
}
