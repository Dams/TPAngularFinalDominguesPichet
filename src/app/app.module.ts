import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { GetCharacterComponent } from './API/get-character/get-character.component';
import { RechercheformComponent } from './API/rechercheform/rechercheform.component';
import { recherche } from './API/service/recherche';
import { FavorisComponent } from './API/favoris/favoris.component';
import { HomeComponent } from './home/home.component';
import { DataComponent } from './data/data.component';
import { StatsComponent } from './API/stats/stats.component';


const appRoute: Routes = [
    {path: "", component:HomeComponent},
    {path:"data", component:DataComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    GetCharacterComponent,
    RechercheformComponent,
    FavorisComponent,
    HomeComponent,
    DataComponent,
    StatsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoute)
  ],
  providers: [recherche],
  bootstrap: [AppComponent]
})
export class AppModule { }
